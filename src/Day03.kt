fun main() {
    fun part1(input: List<String>): Int {
        val bitLength = input.maxByOrNull { it.length }?.length ?: throw Exception()
        val gammaBits = mutableListOf<Int>()
        for (i in bitLength - 1 downTo 0) {
            val ones = input.map { it[i].toString().toInt() }.groupBy { it == 1 }[true] ?: emptyList()
            val bit = if (ones.size < input.size.div(2)) {
                0
            } else {
                1
            }
            gammaBits.add(bit)
        }

        val gamma = gammaBits.joinToString(separator = "").reversed().toInt(2)
        val epsilon = gammaBits.map { it.xor(1) }.joinToString(separator = "").reversed().toInt(2)
        return gamma * epsilon
    }

    fun part2(input: List<String>): Int {
        val bitLength = input.maxByOrNull { it.length }?.length ?: throw Exception()

        fun bitCriteriaFilter(bitCriteria: Int, numbers: List<String>): String {
            var filtered = numbers
            for (i in 0 until bitLength) {
                if (filtered.size == 1) {
                    break
                }
                val grouped = filtered.groupBy { it[i].toString() == "1" }
                val ones = grouped[true] ?: emptyList()
                val zeros = grouped[false] ?: emptyList()
                val map = mapOf(
                    1 to if (ones.size >= zeros.size) {
                        ones
                    } else {
                        zeros
                    },
                    0 to if (zeros.size <= ones.size) {
                        zeros
                    } else {
                        ones
                    }
                )
                filtered = map[bitCriteria] ?: throw Exception()
            }
            return filtered.first()
        }

        val oxygenGeneratorRating = bitCriteriaFilter(bitCriteria = 1, numbers = input)
        val co2ScrubberRating = bitCriteriaFilter(bitCriteria = 0, numbers = input)
        return oxygenGeneratorRating.toInt(2) * co2ScrubberRating.toInt(2)
    }

    // test if implementation meets criteria from the description
    val testInput = readInput("d03_test")
    val input = readInput("d03")

    println(part1(testInput))
    check(part1(testInput) == 198)
    println(part1(input))
    check(part1(input) == 4103154)

    println(part2(testInput))
    check(part2(testInput) == 230)
    println(part2(input))
    check(part2(input) == 4245351)
}
