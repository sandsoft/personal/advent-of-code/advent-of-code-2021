fun main() {
    fun part1(input: List<String>): Int {
        val horizontal = input.map { it.split(" ") }
            .filter { (command, _) -> command == "forward" }
            .fold(0) { sum, (_, unit) -> sum + unit.toInt() }
        val depth = input.map { it.split(" ") }
            .filter { (command, _) -> command != "forward" }
            .fold(0) { sum, (command, unit) ->
                if (command == "down") {
                    sum + unit.toInt()
                } else {
                    sum - unit.toInt()
                }
            }
        return horizontal * depth
    }

    fun part2(input: List<String>): Int {
        var horizontal = 0
        var depth = 0
        var aim = 0

        input.map { it.split(" ") }.forEach { (command, unitString) ->
            val unit = unitString.toInt()
            when (command) {
                "down" -> aim += unit
                "up" -> aim -= unit
                "forward" -> {
                    horizontal += unit
                    depth += aim * unit
                }
            }
        }
        return horizontal * depth
    }

    // test if implementation meets criteria from the description
    val testInput = readInput("d02_test")
    val input = readInput("d02")

    println(part1(testInput))
    check(part1(testInput) == 150)
    println(part1(input))
    check(part1(input) == 2070300)

    println(part2(testInput))
    check(part2(testInput) == 900)
    println(part2(input))
    check(part2(input) == 2078985210)
}
