fun main() {
    fun part1(input: List<String>) = input.map { it.toInt() }
        .windowed(size = 2)
        .count { (prev, cur) -> cur > prev }

    fun part2(input: List<String>) = input.map { it.toInt() }
        .windowed(size = 3)
        .map { it.sum() }
        .windowed(size = 2)
        .count { (prev, cur) -> cur > prev }

    // test if implementation meets criteria from the description
    val testInput = readInput("d01_test")
    check(part1(testInput) == 7)

    val input = readInput("d01")
    check(part2(input) == 1645)
}
