// Alternate version
typealias Matrix<T> = List<List<T>>

fun main() {

    // Taken from https://rosettacode.org/wiki/Matrix_transposition#Kotlin
    fun <T> Matrix<T>.transpose(): Matrix<T> {
        return (0 until this[0].size).map { x ->
            (this.indices).map { y ->
                this[y][x]
            }
        }
    }

    class Card(rawBoard: List<String>) {
        val rows: List<List<Int>>
        val cols: List<List<Int>>

        init {
            val board = rawBoard.map { board ->
                board.split(" ").filter { it.isNotEmpty() }.map { it.toInt() }
            }
            rows = board
            cols = board.transpose()
        }

        fun isBingo(draws: List<Int>): Boolean {
            val rowBingo = rows.map { it.intersect(draws).size == it.size }.any { it }
            val columnBingo = cols.map { it.intersect(draws).size == it.size }.any { it }
            return rowBingo || columnBingo
        }

        fun getUnmarked(draws: List<Int>): List<Int> {
            return rows.flatten()
                .filter { it !in draws }
        }

        override fun toString(): String {
            var representation = ""
            rows.forEach {
                representation += it.joinToString(" ") + "\n"
            }
            return representation
        }

    }

    fun part1(input: List<String>): Int {
        val draws = input.first().split(",").map { it.toInt() }
        val rawBoards = input.drop(1).filter {
            it.isNotEmpty()
        }
            .chunked(5)

        val cards = rawBoards.map { Card(it) }

        for (i in 1 until draws.size) {
            val draw = draws.subList(0, i)
            for (card in cards) {
                if (card.isBingo(draw)) {
                    return card.getUnmarked(draw).sum() * draw.last()
                }
            }
        }
        throw Exception("No solution found")
    }

    fun part2(input: List<String>): Int {
        val draws = input.first().split(",").map { it.toInt() }
        val rawBoards = input.drop(1).filter {
            it.isNotEmpty()
        }
            .chunked(5)

        val cards = rawBoards.map { Card(it) }

        var bingoCount = 0
        val bingoCards = mutableListOf<Card>()
        for (i in 1 until draws.size) {
            val draw = draws.subList(0, i)
            for (card in cards) {
                if (card.isBingo(draw) && card !in bingoCards) {
                    bingoCards.add(card)
                    bingoCount +=1
                    if (bingoCount == cards.size){
                        return card.getUnmarked(draw).sum() * draw.last()
                    }
                }
            }
        }
        throw Exception("No solution found")
    }

    // test if implementation meets criteria from the description
    val testInput = readInput("d04_test")
    val input = readInput("d04")

    println(part1(testInput))
    check(part1(testInput) == 4512)
    println(part1(input))
    check(part1(input) == 71708)

    println(part2(testInput))
    check(part2(testInput) == 1924)
    println(part2(input))
    check(part2(input) == 4245351)
}


